# Repository is archived

Support for RetroFW JilJil Py has ended in favor of the [better C rewrite.](https://codeberg.org/massivebox/retrofw-jiljil).  
This version is not maintained anymore.

# RetroFW JilJil

![Start menu](https://telegra.ph/file/910b1f297d6a1a72d5b1f.jpg) ![Game over - Normal](https://telegra.ph/file/5003fb643c6dbb9e5b72a.jpg) ![A Menu](https://telegra.ph/file/12a0e7652444a5d9d75db.jpg) ![Game Over - Hard](https://telegra.ph/file/f48ee28306b44dbad970a.jpg)

This repo contains the source code for my port of [JilJil](https://www.cavestory.org/pixels-works/jil-jil.php) for (hopefully) all **RetroFW** compatible devices.  
<del>The main highlight of this port is that currently it is the only game publicly available for RetroFW made in **Python** (aside from PyMenu, which is not a game).</del> EDIT: Not true

JilJil is a simple **arcade-style** score based game, where you control a weird **worm** inside an aquarium, and you have to run away from a **cat** that is trying to catch you by the ribbon on your tail. You also have to bounce off of a **lemon** as many times as you can.

While this game is named as a port of JilJil, the actual source code of JilJil is not publicly available, so I decided to rewrite the game from scratch in Python, and I've added some tweaks here and there to spice up the gameplay. The physics are not supposed to be 1:1 to actual JilJil!

## Copyright notice

The original JilJil files and resources are (c) 1997 Studio Pixel.  
This project uses some modified resources and some original files, which are exclusively owned by Studio Pixel.  
NO COPYRIGHT INFRINGEMENT IS INTENDED! If you represent Studio Pixel and you want this project to be gone, please contact me at legal@massivebox.eu.org to get this repository removed and distribution of both the source code and the compiled files completely ceased. Thank you.

## Extra Features

+ **Hard Mode:** The cat has grown so many paws! Will you survive? [Probably not, because this causes the game to crash more often than not. I'm working on it.]
+ **Zen Mode**: Only you and the lemon. The cat's paws won't hurt you here.
+ You can play with the **face buttons** instead of the D-pad if you're left handed or hate D-pads I guess.
+ **Persistent high score:** the high score won't reset each time you power off the console.

## Building a distributable file

Download the repo and run ipker.sh, it will generate a file called `build.ipk` which you should be able to run on all RetroFW devices.