#!/bin/bash

cd data_tar_gz && tar cvzf ../data.tar.gz *
cd ../control_tar_gz && tar cvzf ../control.tar.gz *
cd .. && echo 2.0 > debian-binary
ar rv build.ipk control.tar.gz data.tar.gz debian-binary
rm data.tar.gz control.tar.gz debian-binary