import pygame

BUTTON_UP     =       pygame.K_UP
BUTTON_DOWN   =       pygame.K_DOWN
BUTTON_RIGHT  =       pygame.K_RIGHT
BUTTON_LEFT   =       pygame.K_LEFT
BUTTON_R      =       pygame.K_BACKSPACE
BUTTON_L      =       pygame.K_TAB
BUTTON_A      =       pygame.K_LCTRL
BUTTON_B      =       pygame.K_LALT
BUTTON_X      =       pygame.K_SPACE
BUTTON_Y      =       pygame.K_LSHIFT
BUTTON_SELECT =       pygame.K_ESCAPE
BUTTON_START  =       pygame.K_RETURN
BUTTON_END    =       pygame.K_UNKNOWN
BUTTON_VOL_UP =       pygame.K_2
BUTTON_VOL_DOWN =     pygame.K_1
BUTTON_BRIGHTNESS  =  pygame.K_LEFTBRACKET