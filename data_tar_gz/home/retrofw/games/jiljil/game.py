import pygame, time, threading
import audio, system, keys, sys, physics

def blank_playfield(screen, tiles):

    blockRect = pygame.Rect(64, 64, 20, 20)

    for row in range(2):
        for col in range(14):
            screen.blit(tiles, (21 + 20*col, row*221), blockRect)

    screen.blit(tiles, (208, 22), pygame.Rect(80, 112, 17, 7)) #Hi-
    screen.blit(tiles, (226, 22), pygame.Rect(80, 121, 47, 7)) #Score

    screen.blit(tiles, (226, 213), pygame.Rect(80, 121, 47, 7)) #Score (current)

    screen.blit(tiles, (22, 213), pygame.Rect(64, 95, 23, 7)) #JpLeft
    screen.blit(tiles, (49, 212), pygame.Rect(64, 84, 65, 8)) #JpRight

    screen.blit(tiles, (141, 224), pygame.Rect(80, 16, 48, 7)) #exit->esc


enemy_prints_x = []
enemy_prints_y = []
old_xs = []
old_ys = []
tail_x = 0
tail_y = 0

def start_match(screen, tiles, clock):

    blank_playfield(screen, tiles)
    pygame.display.flip()
    
    game_over = False
    speed_change = 0.1
    deceleration = 0.1

    player_x = 155
    player_y = 213
    player_speed_x = 0
    player_speed_y = 2

    lemon_x = 0
    lemon_y = 0
    lemon_speed_x = 2
    lemon_speed_y = 2

    speed_cap = 2.5
    down_keys = []
    score = 0
    last_score = 0
    best_score = system.read_best()

    global enemy_prints_x
    global enemy_prints_y
    global tail_x
    global tail_y
    global old_xs
    global old_ys

    audio.play_bgm("in_game")
    audio.play_sfx("game_start")

    numbers = pygame.image.load("./assets/images/fpsfont.png")

    tick = 0
    while not game_over:
        tick = tick + 1

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == keys.BUTTON_SELECT or event.key == keys.BUTTON_START:
                    down_keys = []
                    system.pause_menu(numbers, screen, clock)
                down_keys.append(event.key)
            if event.type == pygame.KEYUP:
                if event.key in down_keys:
                    down_keys.remove(event.key)

        if len(down_keys) > 0:
            player_speed_x, player_speed_y = physics.speed_process_keys(down_keys, player_speed_x, player_speed_y, speed_cap, speed_change, deceleration)
        else:
            player_speed_x, player_speed_y = physics.decelerate(player_speed_x, player_speed_y, deceleration, 0.7)
        
        lemon_speed_x, lemon_speed_y = physics.decelerate(lemon_speed_x, lemon_speed_y, deceleration, 0.05)
        lemon_speed_y = lemon_speed_y + 0.05

        player_x = player_x + player_speed_x
        player_y = player_y + player_speed_y
        lemon_x = lemon_x + lemon_speed_x
        lemon_y = lemon_y + lemon_speed_y

        player_x, player_y, player_speed_x, player_speed_y = physics.avoid_entity_outside_borders(player_x, player_y, player_speed_x, player_speed_y, 16, 0.5)
        lemon_x, lemon_y, lemon_speed_x, lemon_speed_y = physics.avoid_entity_outside_borders(lemon_x, lemon_y, lemon_speed_x, lemon_speed_y, 64, 1, False)

        if system.is_point_inside_lemon(player_x, player_y, lemon_x, lemon_y) and last_score < time.time() - 0.5:
            last_score = time.time()
            score = score + 1
            audio.play_sfx("clap")
            lemon_speed_x = lemon_speed_x + player_speed_x
            lemon_speed_y = lemon_speed_y + player_speed_y
            player_speed_x = -player_speed_x * 0.9
            player_speed_y = -player_speed_y * 0.9

        screen.fill((0,0,0))

        screen.blit(tiles, (lemon_x, lemon_y), pygame.Rect(0, 48, 64, 64))
        draw_tail(screen, tiles, player_x, player_y, tick, player_x + player_speed_x, player_x + player_speed_y)

        for n in range(len(enemy_prints_x)):
            if len(enemy_prints_x) > n and len(enemy_prints_y) > n:
                pr_x = enemy_prints_x[n][0]
                pr_y = enemy_prints_y[n][0]
                if len(enemy_prints_x[n]) >= 2:
                    isLeft = enemy_prints_x[n][1]
                    if isLeft:
                        print_rect = pygame.Rect(4, 20, 26, 26)
                    else:
                        print_rect = pygame.Rect(34, 20, 26, 26)
                    if n < len(enemy_prints_x) and n < len(enemy_prints_y):
                        screen.blit(tiles, (pr_x, pr_y), print_rect)
                        if tail_x + 5 >= pr_x and tail_x <= pr_x + 26 - 16 and tick > 250:
                            if tail_y + 5 >= pr_y and tail_y <= pr_y + 26 - 16 and system.get_mode() != "ZEN":
                                game_over = True
        screen.blit(tiles, (player_x, player_y), pygame.Rect(0, 0, 16, 16))

        blank_playfield(screen, tiles)
        system.show_score(276, 204, score, tiles, screen)
        system.show_score(276, 21, best_score, tiles, screen)

        pygame.display.flip()

        system.show_fps(numbers, screen, clock)
        clock.tick(50)

    audio.play_bgm("game_over")
    screen.blit(tiles, (player_x, player_y), pygame.Rect(65, 16, 15, 16)) #caught
    screen.blit(tiles, (53, 51), pygame.Rect(66, 37, 62, 10)) #start
    pressedStart = False
    system.save_best(score)
    pygame.display.flip()
    while not pressedStart:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == keys.BUTTON_SELECT:
                    system.pause_menu(numbers, screen, clock)
                    pygame.display.flip()
                if event.key == keys.BUTTON_START:
                    pressedStart = True
                    break
        system.show_fps(numbers, screen, clock)
        clock.tick(50)
    old_xs = []
    old_ys = []

sparkle_x_no = 0

def draw_tail(screen, tiles, player_x, player_y, tick, movement_x, movement_y):

    global old_xs
    global old_ys
    global tail_x
    global tail_y

    if tick % 2 == 0 or tick % 3 == 0:

        old_xs.insert(0, player_x)
        old_ys.insert(0, player_y)

        if len(old_xs) > 40:
            old_xs = old_xs[:-1]
        if len(old_ys) > 40:
            old_ys = old_ys[:-1]

    for n in range(40):

        if len(old_ys) > n and n % 4 == 0:

            screen.blit(tiles, (old_xs[n], old_ys[n]), pygame.Rect(16 + 16*(n/4), 0, 16, 16))

            if n == 24:
                tail_x = old_xs[n]
                tail_y = old_ys[n]

        if len(old_ys) > n and n == 39:
            
            global sparkle_x_no

            if sparkle_x_no > 3:
                sparkle_x_no = 0

            if tick % 5 == 0:
                sparkle_x_no = sparkle_x_no + 1

            screen.blit(tiles, (old_xs[n], old_ys[n]), pygame.Rect(64 + sparkle_x_no * 16, 48, 16, 16))
            if system.get_mode() == "PAIN" or tick % 25 == 0:
                threading.Thread(target=handle_enemy_print, args=(old_xs[n], old_ys[n])).start()


print_style_left = True
def handle_enemy_print(tail_x, tail_y):

    tail_x_old = tail_x
    tail_y_old = tail_y

    global enemy_prints_x
    global enemy_prints_y
    global print_style_left

    if system.get_mode() == "PAIN":
        time.sleep(0.5)

    x_tuple = (tail_x_old, print_style_left)
    y_tuple = (tail_y_old, print_style_left)

    enemy_prints_x.append(x_tuple)
    enemy_prints_y.append(y_tuple)
    print_style_left = not print_style_left

    if system.get_mode() == "PAIN":
        time.sleep(2)
    else:
        time.sleep(1)

    if x_tuple in enemy_prints_x:
        enemy_prints_x.remove(x_tuple)
    if y_tuple in enemy_prints_y:
        enemy_prints_y.remove(y_tuple)