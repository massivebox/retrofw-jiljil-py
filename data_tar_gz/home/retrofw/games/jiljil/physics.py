import time
import keys, audio

def speed_process_keys(down_keys, player_speed_x, player_speed_y, speed_cap, speed_change, deceleration):

    if keys.BUTTON_UP in down_keys or keys.BUTTON_X in down_keys:
        if player_speed_y - speed_change >= -speed_cap:
            player_speed_y = player_speed_y - speed_change
    else:
        if player_speed_y + speed_change <= 0 and keys.BUTTON_DOWN not in down_keys and keys.BUTTON_B not in down_keys:
            player_speed_y = player_speed_y + deceleration

    if keys.BUTTON_DOWN in down_keys or keys.BUTTON_B in down_keys:
        if player_speed_y + speed_change <= speed_cap:
            player_speed_y = player_speed_y + speed_change
    else:
        if player_speed_y - speed_change >= 0 and keys.BUTTON_UP not in down_keys and keys.BUTTON_X not in down_keys:
            player_speed_y = player_speed_y - deceleration

    if keys.BUTTON_LEFT in down_keys or keys.BUTTON_Y in down_keys:
        if player_speed_x - speed_change >= -speed_cap:
            player_speed_x = player_speed_x - speed_change
    else:
        if player_speed_x + player_speed_x <= 0 and keys.BUTTON_RIGHT not in down_keys and keys.BUTTON_A not in down_keys:
            player_speed_x = player_speed_x + deceleration
                
    if keys.BUTTON_RIGHT in down_keys or keys.BUTTON_A in down_keys:
        if player_speed_x + speed_change <= speed_cap:
            player_speed_x = player_speed_x + speed_change
    else:
        if player_speed_x - player_speed_x >= 0 and keys.BUTTON_LEFT not in down_keys and keys.BUTTON_Y not in down_keys:
            player_speed_x = player_speed_x - deceleration

    return (player_speed_x, player_speed_y)


def decelerate(speed_x, speed_y, deceleration, multiplication_factor):
    if speed_x < 0:
        speed_x = speed_x + deceleration * multiplication_factor
    elif speed_x > 0:
        speed_x = speed_x - deceleration * multiplication_factor
            
    if speed_y < 0:
        speed_y = speed_y + deceleration * multiplication_factor
    elif speed_y > 0:
        speed_y = speed_y - deceleration * multiplication_factor

    return (speed_x, speed_y)

last_cho = 0
def avoid_entity_outside_borders(player_x, player_y, player_speed_x, player_speed_y, sprite_width, muliplication_factor, make_noise=True):

    original_speed_x = player_speed_x
    original_speed_y = player_speed_y

    if player_x < 21:
        player_x = 21
        player_speed_x = -player_speed_x * muliplication_factor
    if player_y < 20:
        player_y = 20
        player_speed_y = -player_speed_y * muliplication_factor
    if player_x + sprite_width > 301:
        player_x = 301 - sprite_width
        player_speed_x = -player_speed_x * muliplication_factor
    if player_y + sprite_width > 221:
        player_y = 221 - sprite_width
        player_speed_y = -player_speed_y * muliplication_factor

    global last_cho
    if (original_speed_x != player_speed_x or original_speed_y != player_speed_y) and make_noise and last_cho < time.time() - 0.5:
        audio.play_sfx("cho")
        last_cho = time.time()

    return (player_x, player_y, player_speed_x, player_speed_y)