import pygame, time, os
import system, game

def main():
     
    pygame.init()

    pygame.mixer.init(size=-8, buffer=256)
    
    logo = pygame.image.load("./assets/images/icon.png")
    pygame.display.set_icon(logo)
    pygame.display.set_caption("PyJilJil")

    pygame.mouse.set_visible(False)
 
    screen = pygame.display.set_mode((322, 242))
    
    tiles = pygame.image.load("./assets/images/JILJIL.png")
    clock = pygame.time.Clock()
     
    screen.fill((0,0,0))
        
    system.startup_animation(screen, tiles, clock)
    while True:
        game.start_match(screen, tiles, clock)

    
if __name__=="__main__":
    main()
