import pygame, time, math, os
import audio, keys, sys, main

def startup_animation(screen, tiles, clock):            
    
    pygame.display.flip()
    audio.play_intro_fx("inhale")

    screen.blit(tiles, (126, 51), pygame.Rect(89, 64, 35, 20)) #Jil
    screen.blit(tiles, (126+35,51), pygame.Rect(89, 64, 35, 20)) #Jil        
    pygame.display.flip()

    audio.play_intro_fx("sneeze_long")

    screen.blit(tiles, (138, 121), pygame.Rect(80, 25, 44, 7)) #Date  
    pygame.display.flip()

    audio.play_intro_fx("sneeze_short")

    screen.blit(tiles, (133, 141), pygame.Rect(64, 103, 55, 9)) #Tortoiseshell 
    pygame.display.flip()

    audio.play_intro_fx("cough_cough")

    screen.blit(tiles, (133, 166), pygame.Rect(66, 37, 62, 10)) #Push start 
    pygame.display.flip()
    pygame.event.get() # Remove previous key strokes

    audio.play_intro_fx("ronf")

    c = pygame.image.load("./assets/images/copyright.png")
    screen.blit(c, (61, 89))
    pygame.display.flip()

    numbers = pygame.image.load("./assets/images/fpsfont.png")
    pressedStart = False
    while not pressedStart:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == keys.BUTTON_SELECT:
                    pause_menu(numbers, screen, clock)
                if event.key == keys.BUTTON_START or event.key == keys.BUTTON_B:
                    pressedStart = True
                    break
        show_fps(numbers, screen, clock)
        clock.tick(50)

def restart_game():
    audio.stop()
    main.main()

def set_mode(name):
    os.environ["JILJILMODE"] = name
    restart_game()

def get_mode():
    return os.environ.get('JILJILMODE', 'NORMAL')

def pause_menu(numbers, screen, clock):
    img = pygame.image.load("./assets/images/pause.png")
    screen.blit(img, (0, 0))
    pygame.display.flip()
    audio.pause()
    pressedStart = False
    while not pressedStart:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == keys.BUTTON_SELECT:
                    sys.exit()
                if event.key == keys.BUTTON_START:
                    pressedStart = True
                    audio.play()
                    break
                if event.key == keys.BUTTON_R:
                    restart_game()
                if event.key == keys.BUTTON_X:
                    set_mode("ZEN")
                if event.key == keys.BUTTON_B:
                    set_mode("PAIN")
                if event.key == keys.BUTTON_A:
                    set_mode("NORMAL")
                if event.key == keys.BUTTON_Y:
                    save_best(0, True)
                    restart_game()
        show_fps(numbers, screen, clock)
        clock.tick(50)

def show_fps(numbers, screen, clock):
    fps = int(clock.get_fps() + 0.5)
    digits = [int(x) for x in str(fps)]
    screen.blit(numbers, (305, 4), pygame.Rect(digits[0]*8, 0, 8, 8)) #left
    if len(digits) > 1:
        screen.blit(numbers, (313, 4), pygame.Rect(digits[1]*8, 0, 8, 8)) #right
    pygame.display.flip()

def points_distance(x1, y1, x2, y2):
    return math.sqrt((x2- x1)**2 + (y2-y1)**2)

def is_point_inside_lemon(point_x, point_y, lemon_x, lemon_y):
    return points_distance(point_x, point_y, lemon_x+32, lemon_y+32) < 32

def read_best():
    with open("bestscore.txt") as f:
        return int(f.read())

def save_best(score, force=False):
    if score > read_best() or force:
        with open('bestscore.txt', 'w') as f:
            f.write(str(score))

def show_score(score_x, score_y, score, tiles, screen):
    digits = [int(x) for x in str(score)]
    for _ in range(3-len(digits)):
        digits.insert(0, 0)
    digit_no = 0
    for digit in digits:
        screen.blit(tiles, (score_x + (8 * digit_no), score_y), pygame.Rect(digit*8, 112, 8, 16))
        digit_no = digit_no + 1