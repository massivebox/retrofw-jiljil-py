import pygame, time, audio

def play_sfx(name):
    audio = pygame.mixer.Sound("./assets/sfx/" + name)
    audio.play()

def play_intro_fx(name):
    audio = pygame.mixer.Sound("./assets/sfx/" + name)
    lenght = audio.get_length()
    audio.play()
    time.sleep(2*lenght)


playing_bgm_name = ""

def play_bgm(name):
    global playing_bgm_name
    if name != playing_bgm_name or not pygame.mixer.music.get_busy():
        if not pygame.mixer.music.get_busy():
            pygame.mixer.music.stop()
        pygame.mixer.music.load("./assets/bgm/" + name)
        pygame.mixer.music.play(loops=-1)
        playing_bgm_name = name

def pause():
    pygame.mixer.music.pause()
def stop():
    pygame.mixer.music.stop()
def play():
    pygame.mixer.music.unpause()
    pygame.mixer.music.play()